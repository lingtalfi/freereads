<?php 

namespace App\Tests\Entity;

use App\Entity\Author;
use App\Entity\Book;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    public function testGetBooksReturnsEmptyCollectionByDefault()
    {
        $author = new Author();
        $this->assertEmpty($author->getBooks());
    }

    public function testAddBookAddsBookToCollection()
    {
        $author = new Author();
        $book = new Book();
        $author->addBook($book);
        $this->assertContains($book, $author->getBooks());
    }

    public function testAddBookDoesNotAddDuplicateBook()
    {
        $author = new Author();
        $book = new Book();
        $author->addBook($book);
        $author->addBook($book);
        $this->assertCount(1, $author->getBooks());
    }

    public function testRemoveBookRemovesBookFromCollection()
    {
        $author = new Author();
        $book = new Book();
        $author->addBook($book);
        $author->removeBook($book);
        $this->assertNotContains($book, $author->getBooks());
    }

    public function testRemoveBookDoesNothingIfBookNotInCollection()
    {
        $author = new Author();
        $book = new Book();
        $author->removeBook($book);
        $this->assertEmpty($author->getBooks());
    }
}