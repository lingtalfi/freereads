<?php

namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testGetId()
    {
        $user = new User();
        $this->assertNull($user->getId());
    }

    public function testGetEmail()
    {
        $user = new User();
        $user->setEmail('test@example.com');
        $this->assertEquals('test@example.com', $user->getEmail());
    }

    public function testGetRoles()
    {
        $user = new User();
        $this->assertContains('ROLE_USER', $user->getRoles());
    }

    public function testSetRoles()
    {
        $user = new User();
        $user->setRoles(['ROLE_ADMIN']);
        $this->assertContains('ROLE_ADMIN', $user->getRoles());
    }

    public function testGetPassword()
    {
        $user = new User();
        $user->setPassword('password');
        $this->assertEquals('password', $user->getPassword());
    }

    public function testGetPseudo()
    {
        $user = new User();
        $user->setPseudo('testuser');
        $this->assertEquals('testuser', $user->getPseudo());
    }

    public function testAddUserBook()
    {
        $user = new User();
        $userBook = $this->createMock('App\Entity\UserBook');
        $user->addUserBook($userBook);
        $this->assertContains($userBook, $user->getUserBooks());
    }

    public function testRemoveUserBook()
    {
        $user = new User();
        $userBook = $this->createMock('App\Entity\UserBook');
        $user->addUserBook($userBook);
        $user->removeUserBook($userBook);
        $this->assertNotContains($userBook, $user->getUserBooks());
    }
}